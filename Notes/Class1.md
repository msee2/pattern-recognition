# Administrative stuff

Prof: Felipe Meza

## Documentation and materials

https://tinyurl.com/patrones2020-2q

## Syllabus

1. Foundations
2. Deep learning
3. Neural Networks

## Evaluation

1. Research 60% (4 short evaluations)
2. Final project 40% (10% proposal - 30% completion)

> Solutions emailed to the professor.

# Introduction - Foundations

We are looking for patterns on data, so called "structural patterns".

* ADALINE: First neural network - predicts binary digits within a bit stream.

* XOR Problem: The first proof-of-concept of neural networks was destroyed by 
this problem, which demonstrates that neural networks were not able to 
replicate the behaviour of a logical gate.

* Multi-layered perceptron: backpropagation as the optimisation technique.

## Fields

Artificial Intelligence:

* Natural Language Processing (NLP)
* Computer Vision (CV)

Machine learning:

* Classification
* Regression
* Deep learning

> Inspired by human brain

Deep learning:

* Application-specific to identify patterns invisible for humans

## Definition of Machine Learning

1. Mechanisms which convert data to meaningful knowledge
2. Programs enhanced by experience
3. Machines not explicitly programmed to do a task which were not intended for.

## Statistics vs Machine Learning

* Statistics: from data, **infer** conclusions. It looks for variable relationship (mathematics).
* Machine learning: from data, we **generalise** (computer science).

## Machine Learning vs Traditional Programming

**Traditional Programming**

Inputs:

* Data: mass, coeffs
* Program: equation

Outputs:

* Output: the result (energy, ...)

**Machine Learning**

Inputs:

* Data: mass, coeffs
* Output: the result (energy, ...)

Outputs:

* Program: structural patterns (equations, weights, etc...)

> Proxies: approximation of the equations and relation amongst variables.

## Typical pipeline

1. Preprocessing: enhance the data quality - we need to guess relationships.
Also, we have to cure the data (fill missing data), remove outliers,
noise, etc. **80/20** rule -> 80% data curation, 20% rest of stages.
2. Feature extraction
3. Classification
4. Validation: some metrics depending on classification/regression: test 
dataset.

## Learning types

**Supervised learning:**

* Labeled data: classes (outputs), features (inputs)
* Predicts an output

Data: $\{x_i, y_i\}^N_{i=1}$

each characteristic is expressed as: $x^{(j)}$, where $j$ is the dimension.

Subtypes:

* Classification
* Regression

**Unsupervised learning:**

* Non-labeled data: we only have features
* Finds a hidden structure

We only have data: $\{x_i\}^N_{i=1}$

**Reinforcement learning:**

* Decision process
* Rewarding system
* Learns actions

It is about an *agent* interacting with an *environment*, which do some actions in
each *state*. Some actions are *rewarded* and others are *punished* until 
learning a *policy*.

Subtypes:

* Clustering

> Instances: samples

> The features form a matrix called: attribute matrix

> The classes form a vector

## Design Methodology

![](Class1_SupervisedPipeline.png)

## Metrics

* Accuracy
* Curva ROC
* Confusion matrix
* MSE
* Logarithm losses
* $R^2$

## Tools

> Conda: it is like PIP and Containers merged