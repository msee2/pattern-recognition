# Machine Learning v2

Everything before Deep Machine Learning...

## Linear regression

> `requires_grad=True` switches on the gradient computability.

> `backward` computes the gradient recursively backwards.

**Regularisation** is an hyper-param.

Weights and biases are often randomly initialised: `manual_seed()`.

To convert from numpy to tensor: `tensor.from_numpy(numpy_array)`

To determine the error: `Minimum Square Error`. This is the **loss**.

Then compute the gradient descend to the loss.

`grad_zero()`: resets

Each adjustment iteration is known as: epoch.

## SVM

The issue with linear models is that they require a lot of iterations. The SVM
is smarter when looking for the optimal solution.

The idea es to generate a plane which maximises the margin which separates 
two classes.

![](Class3_SVM.png)

The support vectors are the innermost (near of the decision line) outer points.

**Prediction**

$$
y = \text{sign}(wx-b)
$$

**Model**

$$
f(x) = \text{sign}(w^* x - b*)
$$

The distance between support vectors (support width) should be maximised:

$$
\min||w|| = \min \frac{1}{2} ||w||^2
$$

where

$$
||w|| = \sqrt{\sum_{j=1}^{D} (w^{(j)^2})}
$$

The vectors are computed: $wx + b$.

> The square of the norm is used for facilitating the gradient optimisation.

### Pros:

1. Good precision
2. Good for small datasets

### Cons:

1. They are slow with large datasets
2. Susceptible to noise

Also, SVM uses a regularisation criteria. There is a parameter C. Smaller it 
is, more effective but slower.




## Multi-class models

They are the most common cases. The most common strategy is *one vs the rest*. 
For each class, the algorithm computes the decision line of the class under 
analysis against the rest. At the end, each decision line (vector) is 
substracted to get the effective division.

---

Some reminders:

* Linear models: $\alpha$
* SVM: C

The L1 regularisation is used when it's clear that only some attributes are important.

Linear models are fast. The scale well and easy to understand.

The coefficients are not easy to understand.

## kNN: k Nearest Neighbours (Classification)

k: Hyper-parameter. It defines the number of neighbours to take into account.

Depending on the classes of the neighbours, the final class will be the most predominant.

> It is not linear

> Low **k** implies more complex models and it makes it susceptible to 
overfitting.

When finding the optimal hyper-parameter (in general), a plot of two lines: 
test accuracy and train accuracy is often used.

![](Class3_hypertuning.png)

kNN can be also used for regression.

![](Class3_kNNregresion.png)

### Pros

* Easy to understand
* Good results without too much effort

### Cons

* Training may be slow for large datasets

## Naïve Bayes

$$
P(A|B) = \frac{P(A) \times P(B|A)}{P(B)}
$$

Three types:

* BernoulliNB: Binary data
* MultinomialNB: Data which represents a counting
* GaussianNB: Gaussian distribution of the data

It is a statistical model: average of each attribute for each class, average value of the attribute and the std for each class.

### Pros

* Huge datasets
* Fast in training and prediction
* Really good for tagging

--- 

GaussianNB: it is used for high-dimensional data, whereas the other on sparse data.

## Decision Trees

It is based on IF/ELSE and decision around each case. Each decision node is 
called: leaf.

> It is not a linear model.

They can be used for classification or regression.

Hyper-parameters:

* `max_depth`
* `max_leaf_nodes`
* `min_samples_leaf`

### Pros

* Easy to visualise
* It doesn't require too much preprocessing

### Cons

* It is highly susceptible to overfitting.

There are some workarounds to mitigate the overfitting:

* Pre-prunning: stop decision tree creating when reaching the max number of leaves or the depth.
* Pruning: Remove or collapse nodes which contains few information..

`tree.feature_importantes_`: allows to know which features influences on the 
decision.

## Random Forest

It belongs to "assemble learning methods" (from several weak methods, it 
makes an stronger).

It is a proposal for avoiding overfitting on decision trees.

They are composed by several decision tree. The results from the several trees are weighted, reducing the overfitting.

It also works for classification or regression.

Hyper-parameters:

* `max_features`
* `n_estimators`: number of trees

For classification, the result is computing thru voting.

## Kernel SVM

It is an extension for SVM for multi-dimensional data. It introduces non-linearity to the model.

For example, it introduces a new dimension:

![](Class3_KernelSVM.png)

> The kernel introduces new criteria or non-linearities to the model.

Types of kernels:

* Polinomial: computes all the possible polinomials.
* Radial Basis Function: or Guassian kernels.

* C: regularisation
* gamma: gaussian control

![](Class3_KernelSVM_2.png)

* High C: more points influences on the decision

### Pros:

* It helps for non-linear data
* Works well in both cases: low and high dimensional data.

### Cons:

* Computationally more expensive
* Pre-processing is mandatory

> There are some notebooks available!

# Research N. 3

Individual

* Identify a new dataset Class2: with a challenge. Look for papers that uses it.
* Do a benchmark: 3-5 algorithms (2 not seen). ADA-boost.
* Summarise the results in a table -> explain why


