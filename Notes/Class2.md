# Preprocessing

* $\vec{X}$: Attributes
* $Y$: Classes

$$
f(\vec{X}) \to Y
$$

## Pre-processing

This is the first block of the pipeline in any Machine Learning process.

Tasks:

* Identify incomplete, wrong and irrelevant components from the dataset, 
removing, modifying or replacing them: such as `NaN`, `0`s.
* Transform raw data into other formats for easing the data mining.
* Order Reduction, such as PCA, Eigen values...

Known Terms:

* Data preparation
* Data cleaning
* Pre-processing
* Cleansing
* Wrangling

### Data preparation

Libraries:

* Pandas: concept of dataframes and widely used for data preparation.
* Numpy.

It usually takes 80-90% of the time.

### Normalisation and Standardisation

Adapt the probability distributions to a standard distribution.

**Normalise**: make the data to be within a range of 0-1.

$$
z = \frac{x - \min(x)}{\max(x) - \min(x)}
$$

**Standadise**: have a $\mu = 0$ and $\sigma = 1$

$$
z = \frac{x-\mu}{\sigma}
$$

> ANNs are highly susceptible to the normalisation.

### Exploratory Data Analysis (EDA)

A set of quantitative and qualitative methods to understand a dataset without 
assuming any facts.

* Visualisation: statistical summary
* Exploration: to look for any relation of any attribute with the target classes.
* Dispersion diagrams to see possible clusters.

TITANIC: a common dataset from the TITANIC accident. It is trivial.

Things that we may see:

* Distribution: it helps to select a proper statistical model.

### Missing values

Some techniques:

* Delete instances
* Delete attributes
* Compute the mean, modal, meadian
* Regression

> Median: not influenced by outliers

With Pandas:

```python
# Drop col whose all of its attributes are missing
df.dropna(axis=1, how='all')
# Any col with a missing element
df.dropna(axis=1, how='all')
# Fill with the median
df.fillna(df.median())
```

### Outliers

Depending on the dataset, they should be deleted or preserved (if it may 
happen).

Some techniques:

* Using the standard dev
* Percentiles

**3-$\sigma$**

A.k.a 68-95-99 rule

It removes all data after $\pm 3\sigma$.

### Data unbalance

When the classes has a way different number of instances.

This introduces a bias to the majorities.

Techniques:

1. Sampling: sub-sampling/over-sampling

For sampling, it depends on the number of instances.

2. F1 score, precision/specificity, recall/sensitivity

It is based on the precision of the data. They also help to identify 
problematic classes.

3. Synthetic data generation: it needs to make sense. It may be complex.
For example: Adversarial Neural Networks.

4. Clustering and decomposition: if we can split classes into smaller ones. 

> Over-sampling: it may not be recommended to some data sets.

### Data transformation

It is related to the categorical variables. 

Common technique: **One hot encode** -> assign numbers to the categorical 
variables (binary vectors).

It helps to generate a vector operable with the other matrices.

## Repositories

* Google's Datasets
* Awesome Public Datasets
* Kaggle Datasets (sets to be solved and they pay for it)
* UCI ML Repo

## Preprocessing example

Import needed libraries:

![](Class2_NotebookStart.png)

Read data: example for the congress voting 198x

```python
df = pd.read_csv("filename.data")
```

See the first values:

```python
df.head()
```

Define the column labels

```python
column_names = ["myvector"]
# Reread the file
df = pd.read_csv("filename.data", names = column_names)
```

Replace values:

```python
# Assign valid values
df.replace(to_replace='y', value=1, inplace=True)
df.replace(to_replace='n', value=0, inplace=True)
df.replace(to_replace='?', value=np.nan, inplace=True)

# Fill NaNs
df.fillna(df.mean(), inplace=True)

# We can make it uniform
df = df.round()
df.head()
```

## Exercise

```python
from statistics import mean, median
```

Let's define a data set

```python
# Outlier - 80
data = [17, 80, 22, 16, 20]
mn = mean(data)
print("Mean", mn)
```
> Mean 31 

It should be like ~19. The mean is highly influenced by the outliers

```python
# Outlier - 80
data = [17, 80, 22, 16, 20]
mn = median(data)
print("Median", mn)
```
> Median 20

It is a value which is closer than the expected value.

---

# Research I

Look for a Class 2 dataset and analyse it for preprocessing with all the 
stuff seen above.

Justify why it is class 2.

Use a Jupyter Notebook.

Explain all the steps: go step by step, show results, etc...

> Individual: Two weeks

# Learning I

The goal is to identify the right algorithm given the kind of problem and our data.

It is likely to be chosen through a benchmark with a heuristically selected
algorithms.

Also, we have to tune the algorithm through the parameters.

> Hyperparameters: they are our knobs

## Supervised Learning

> Training set: 80%, Testing set: 20%

We want to *predict* the class from the attributes of an instance.

Types:

* Classification: predict a class
* Regression: predict a continuous number

> **Decision boundary**: Where the decision is switched

**Generalisation**

Capability of predict the output correctly.

### Common issues

**Overfitting**

When your model is highly complex. The consequence is that for new data, it 
won't generalise it properly, since the model is too tailored to the training 
data.

Symptoms:

* Good accuracy on training set
* Poor accuracy on testing set

**Underfitting**

The model is too simple and doesn't describe or generalise well.

Symptoms:

* Accuracy on both sets will be bad (low).

> We need to balance

![](Class2_issues.png)

Accuracy metric:

![](Class2_issues2.png)

## Linear models

$$
y = x[0] \times w[0] + x[1]\times w[1] + ... + x[p] \times w[p] + b
$$

* $w$ : weight
* $x$ : input
* $b$ : bias

$$
f_{\bold{w},b}(\bold{x}) = \bold{w}\bold{x}+b
$$

> Missing: COST function

### Regression

The idea is to minimise the MSE (mean square error) by choosing the $\bold{w}$ and $b$.

```python
lr = LinearRegression().fit(xtrain, ytrain)
lr.score(xtrain, ytrain)
lr.score(xtesting, ytesting)
```

**Regularisation**

Influence on the model to avoid overfitting by assigning penalties to the 
$\bold{w}$.

Types:

* LASSO: L1
* RIDGE: L2

![](Class2_Regularisation.png)

where $C$ is an hyperparameter. It introduces the regularisation to make the
model fit or balanced.

> **parameter**: values inside of the model which we cannot control. For example $\bold{w}, b$

**L1**

We can get $\bold{w}$ penaltied to zero.

Also, it reduces the complexity of the model.

**L2**

It chases the penalties near to zero, so it minimises the impact. However, 
we don't reduce the computational impact of it.

> $\alpha = C$

### Classification

The mathematical framework is the same to the regression. Nevertheless, it 
introduces the **decision boundary**. 

Two types:

* Logistic Regression
* SVM

**Logistic regression**

> It is not a regression!

$$
f(x) = \frac{1}{1+e^{-x}}
$$

We want to have the 0.5 where $x=0$. It is also called *sigmoid*. $f(x)$ is the
CDF.

$$
f_{w,b} = \frac{1}{1 + e^{-(wx+b)}}
$$

We want to maximise the *likelihood*

![](Class2_LogRegression.png)

> Next week: No virtual lessons

* Non trivial:

- With missing values
- With NaN
- Without col names
- See the prob dist
