# Tutorial de Google Colaboratory

Por: Luis G. Leon-Vega.

## 1. Abrir el webapp

Visitar el sitio del webapp de Google Colaboratory Lab:

https://colab.research.google.com

Es posible que sea requerido ingresar sus datos de la cuenta de Google para
efectos de guardar el Notebook.

## 2. Crear un nuevo notebook

Cuando se ingresa, saltará un *pop-up* con acciones a realizar, como se 
ilustra a continuación.

![](img/gc_step1.png)

Para crear un nuevo notebook, se puede dar click a *NEW NOTEBOOK*. Si esta 
ventana no aparece, puede igualmente crear un nuevo *Notebook* en el menú 
principal, dando click a `File > New notebook`.

![](img/gc_step1_1.png)

## 3. Conectar al entorno y guardar el notebook

Después de dar la orden de creación, aparecerá un nuevo *Notebook*. 

![](img/gc_step2.png)

Sin  embargo, este aún no está corriendo sobre ningún entorno. Para conectar 
a un entorno, damos click en *Connect*, tal como se ilustra en la figura de 
arriba.

Además, podemos dar nombre al *Notebook* dando click al nombre plantilla 
*Untitled0.ipynb*. Eso habilitará el editor de texto. Para guardar el título, 
basta con pulsar *Enter*/Intro.

> Los *Notebooks* se guardan en nuestro Google Drive, en una carpeta llamada 
*Colab Notebooks*. Para abrirlos, basta con dar click sobre el nombre y
seleccionar la opción de Abrir con Google Colaboratory

## 6. Iniciando con un Notebook

En Google Colabs existen dos tipos de celdas: las de **código** y las de 
**texto**.

![](img/gc_step4.png)

Los botones de arriba ayudan para agregar nuevas celdas de cada tipo.

A la izquierda de cada celda, aparecen botones de ayuda para manipular dicha celda:

![](img/gc_step4_1.png)

Ahora, las celdas de **código** almacenan texto que es ejecutable, sea en 
Python, Bash u otros. Las celdas de **texto** almacenan texto que no es 
ejecutable. Sirven para explicar las celdas de código. Generalmente, estas 
celdas se escriben en lenguaje *Markdown* (para más info: 
[guía de markdown](https://guides.github.com/features/mastering-markdown/)).

## 5. Importar bibliotecas y entornos

Google Colabs tiene algunas bibliotecas y entornos que son útiles para la 
mayoría de actividades que realizamos en Jupyter Lab / Jupyter Notebook.

![](img/gc_step5.png)

Por ejemplo, tenemos el modo `%pylab`, que incluye *numpy* y *matplotlib*.
Para ver más opciones, puedes digitar `%` en una celda de comando y esta 
desplegará un menú para autocompletar con todas las opciones disponibles.

## 6. Ejecutando código

La ejecución de código es igual que en Jupyter Notebooks y en Python. No 
obstante, hay un par de atajos que sirven para minimizar la cantidad de clicks
en el GUI.

1. Para ingresar una nueva dentro de la misma celda para agrupar código: **Enter**
2. Para crear una nueva celda de código debajo de la actual: **Shift + Enter**
3. Ir a la celda de arriba o a la de abajo: **Up, Down Arrows** (flechas de 
arriba y abajo).

## 7. Preparando nuestro entorno de Preprocessing

Para crear un entorno similar al de la clase 1, solamente se deben importar 
las bibliotecas respectivas.

Exceptuando `Scikit Learn`:

```python
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
```

Por ejemplo:

![](img/gc_step7_1.png)

Para el caso de `Scikit Learn`, podemos usar el siguiente ejemplo:

```python
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import plot_roc_curve
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import matplotlib.pyplot as plt

X, y = make_classification(random_state=0)
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=42)

svc = SVC(random_state=42)
svc.fit(X_train, y_train)
rfc = RandomForestClassifier(random_state=42)
rfc.fit(X_train, y_train)

svc_disp = plot_roc_curve(svc, X_test, y_test)
rfc_disp = plot_roc_curve(rfc, X_test, y_test, ax=svc_disp.ax_)
rfc_disp.figure_.suptitle("ROC curve comparison")

plt.show()
```

Por ejemplo:

![](img/gc_step7_2.png)

## 8. Guardando el notebook

Una vez seguidos los pasos del punto 3., el notebook puede guardarse rápidamente con **Ctrl + s**.
