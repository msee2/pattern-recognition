%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%2345678901234567890123456789012345678901234567890123456789012345678901234567890
%        1         2         3         4         5         6         7         8

\documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out
                                                          % if you need a4paper
%\documentclass[a4paper, 10pt, conference]{ieeeconf}      % Use this line for a4
                                                          % paper

\IEEEoverridecommandlockouts                              % This command is only
                                                          % needed if you want to
                                                          % use the \thanks command
\overrideIEEEmargins
% See the \addtolength command later in the file to balance the column lengths
% on the last page of the document

\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[T1]{fontenc}
\usepackage{listings}

% The following packages can be found on http:\\www.ctan.org
%\usepackage{graphics} % for pdf, bitmapped graphics files
%\usepackage{epsfig} % for postscript graphics files
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{mathptmx} % assumes new font selection scheme installed
%\usepackage{amsmath} % assumes amsmath package installed
%\usepackage{amssymb}  % assumes amsmath package installed

\title{\LARGE \bf
A brief introduction to Hierarchical Bayesian Models (HBM) for Machine Learning*
}

\author{Luis G. Leon-Vega$^{1}$% <-this % stops a space
\thanks{*This work is part of an academic work for the M.Sc in Electronics 
Engineering}% <-this % stops a space
\thanks{$^{1}$ L. Leon-Vega is with Instituto Tecnológico de Costa Rica}%
}


\begin{document}



\maketitle
\thispagestyle{empty}
\pagestyle{empty}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}

HBMs base their foundations on probabilistic methods, exploiting the 
properties of the Bayesian Probability. They are widely used for numerical
regression, and their performance can be improved when the data have information
about the error and tolerances. This paper intends to be an introductory brief
to HBMs, introducing the concepts under the hood, their basis, properties
and usages for Machine Learning (ML) applications.

\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{INTRODUCTION}

HBMs are based on the Bayesian Probability. As a recall, the Bayesian Theorem 
allows to infer the probability of an event A given the occurrence B from the
probabilities of observing A and B and the likelihood  of observing B given A\cite{ref0}.
Mostly, Bayesian Models base their principle on the conditional probability. 
The mathematical description of the Bayes Theorem\cite{ref1} is presented in the
equation \ref{eqn:bayesian_theorem}.

\begin{equation}
  P(A|B) = \frac{P(B|A) P(A)}{P(B)}  
  \label{eqn:bayesian_theorem}
\end{equation}


where $P(A|B)$ is the likelihood of the occurrence of the event $A$ given the
event $B$, as it was described above. A restriction to the Bayes theorem is that
the event B cannot be an impossible event, meaning that $P(B) \neq 0$.

HBMs can be seen as a hierarchy of bayesian inferences, based on the Bayes
Theorem, where the mean of one of the nodes close to the observations are 
described by a normal distribution, which at the same time, its mean is also 
described by another normal distribution.

The applications of this method are widely spread for regression applications

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Scope of HBMs}

\cite{ref0} presents the HBMs as a tool to analyse the market given the
evidences and adjust  the prices according to the observations when having several
options (parameters). In this way, the HBMs help the businesses to take 
decisions about adjusting the prices predicting the possible impact on the
market and the potential profit.

Analysing deeply the example provided by \cite{ref0}, HBMs are powerful for
performing experiments when there are data related to observations, their linked
errors as a result of using several parameter configurations. Mainly, for that
reason, the HBMs are a great tool for market analysis. Nevertheless, its use can
be extended to any other application which accomplish with those requirements:
observations with tolerances and parameter configurations with also errors linked
to them. For example, the impact of changing the price of a product (parameter)
and the investment on advertisement on the number of units sold. This example
involves one random variable (observation) and two parameters (the price and 
the investment in advertisement).

Moreover, since their hierarchical structure, it is possible to represent chains
of events which involve as a consequence of their occurrences of the previous
events. Taking the last example as an example, the impact on the overall company
can be also an observable and the number of units sold of a product can also work
as a parameter, which depends at the same time as the two other parameters 
already mentioned.

HBMs can lead to better results compared to other classical methods because of 
the integration of the errors in both the observations and the parameters as a
inherited feature of using probabilistic. It helps to add uncertainty to
predictions which work as a flag to evaluate the effectiveness of the HBM
\cite{ref0}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{HBM foundations}

HBMs is built on top of the Bayesian inference principles. This means that HBMs
can be understood from analysing the Bayesian inference concepts in order to 
understand the meaning of each element of the Bayes Theorem equation.

\subsection{Bayesian inference foundations}

The Bayesian inference framework gives a meaning to all the terms of the 
equation \ref{eqn:bayesian_theorem}\cite{ref2}:

\begin{itemize}
  \item $A$ will be now called Hypothesis.
  \item $B$ is the evidence
  \item $P(A)$ is called the \emph{prior probability} which is the probability of 
  the hypothesis before analysing the evidence.
  \item $P(E)$ is the marginal evidence probability or also \emph{model evidence}.
  \item $P(A|B)$ is called the \emph{posterior} probability, which is now the 
  probability of the hypothesis after analysing the evidence.
  \item $P(B|A)$ is called the likehood to find the evidence given an hypothesis.
\end{itemize}

Connecting the concepts of the Bayesian inference to the HBMs, from an ML 
perspective, HBMs is a method to estimate infer the most likely 
hypothesis from the evidence. In the HBMs context, the hypothesis is often renamed
to parameter and the evidence as an observation. Thus, the HBMs estimates the 
parameter from the observations by using the posterior probability. 

In principle, according to the equation \ref{eqn:bayesian_theorem}, it is 
required to now the prior probability and the likelihood in order to compute the
posterior. To fulfil this requirement, the likelihood is assumed to be a 
normal probability density, which was explored during the training, as well as 
the other parameters based on the input data \cite{ref0}.

\subsection{Bayesian-based models}

In ML, there are three types of models which rely on Bayesian Inference. The 
way to classify them is basically given by the relationship between the 
observations and the parameters \cite{ref4}:

\begin{itemize}
  \item \textbf{Unpooled}: there is a match between the number of observed
  classes and the number of parameters: each class is analysed by a parameter
  individually.
  \item \textbf{Pooled}: all the classes are analysed by a single parameter.
  It can be also seen as treating all the classes as an average.
  \item \textbf{Partially-pooled}: it can be seen as an unpooled case, but the
  parameters represent also subset from distributed amongst a distribution.
\end{itemize}

To describe how these types work at the model level, consider the linear model
from equation \ref{eqn:linearbase}.

\begin{equation}
  y_i = \alpha + \beta x_i + \epsilon_i
  \label{eqn:linearbase}
\end{equation}

where $y_i$ is the \emph{i-th} observation, $x_i$ is the \emph{i-th}
measurement, $\beta$ is a weight parameter to adjust the slope to control the
relevance of the measurement on the observed data, $\alpha$ is the bias 
parameter, and $\epsilon_{i}$ is the \emph{i-th} error associated to the 
observation.

The model presented in equation \ref{eqn:linearbase} represents a case of
\emph{pooled} analysis, since the parameters $\alpha$ and $\beta$ are treated 
as an average (single value), without the possibility of analysing the effects of
each class. Figure \ref{fig:pooled} illustrates the pooled model.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{../img/pooled_model.png}
  \caption{Pooled model. The factors are collapsed into a single $\theta$.}
  \label{fig:pooled}
\end{figure}

On the other hand, the \emph{unpooled} will treat the observation either by
assigning different weights to the inputs or having a different bias. In
equation \ref{eqn:unpooled_framework}, the bias is changed according to the
observation for a determined input.

\begin{equation}
  y_i = \alpha_{j[i]} + \beta x_i + \epsilon_i
  \label{eqn:unpooled_framework}
\end{equation}

where $\alpha_{j[i]}$ is the $j-th$ class of observation. This can be seen as
the effect of having the $j$ factor on the observation $i$. In this case, $j$
can be any index from the set of factors (all the factors which can intervene
in the analysis). Figure \ref{fig:unpooled} illustrates how the parameters are
also linked to each factor ($k$ is the factor).

\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{../img/unpooled_model.png}
  \caption{Unpooled model. Each factor is represented by a parameter $\theta$.}
  \label{fig:unpooled}
\end{figure}

For \textbf{Partially-pooled}, the equation become more complex, dealing now
with an observation within a probabilistic distribution 
$y ~ N(\hat{y}_j, \sigma_y^2)$, where $\hat{y}_j$ is a transformed variable, which
is the value of $\alpha$ for the factor (or class) $j$.

For the case of $\alpha$, it is also distributed in another probabilistic
distribution: $\alpha_j[i] ~ N(\mu_a, \sigma_a^2)$. Figure 
\ref{fig:partial_pooled} shows a graphical representation of this chain of
parameter dependency. This paper will describe in detail how this works in 
the following sections.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{../img/partial_pooled_model.png}
  \caption{Partially pooled model. The parameters $\theta$ are also
  probabilistic distributions.}
  \label{fig:partial_pooled}
\end{figure}

\subsection{Hierarchicalisation of the bayesian inference framework}

The innovation of HBMs relies on the fact of putting several bayesian inferences
in a Hierarchical fashion. It involves having intermediate inferences which 
don't have a clear meaning. Nevertheless, they work as a learning knob for the 
whole HBM.

To describe the hierarchy, each node has its own probability density function 
(PDF). The most common PDFs utilised for HBMs is the normal distribution. To 
connect one node from one layer to another node from another layer, the mean
is often the way of interconnection. The means of the layers closer to the 
observations are often got from the PDF of the nodes of the following layer.
The process is repeated until having a common node, whose PDF is called the 
\emph{hyper-prior} and its mean value is also an \emph{hyper-parameter}; which are 
commonly selected by the user\cite{ref3}. Thus, the HBM looks for the posterior 
probability by exploring the prior probabilities.

\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{../img/hbm_hierarchy.jpg}
  \caption{HBM graphical representation. The first level is the closest to the 
  observations (called also registry). The mean will be normally distributed in
  the second level. At the same time, the parameters of the second level distribution 
  are distributed within a third level}
  \label{fig:hbm_hierarchy}
\end{figure}

Figure \ref{fig:hbm_hierarchy} shows how the HBM graphical representation looks
like. This example shows a three-layer HBM mainly composed of 3 inputs ($R_i$), 
whose distributions are centred in $\mu$. $\mu$ is a parameter which is 
distributed in the prior distribution on the second layer. At the same time, the
second layer distribution has its parameters (which are in real 
hyper-parameters) defined by two different hyper-priors.

The hyper-priors which define the behaviour of the top prior distribution are 
defined and tuned by the user, using a distribution for both: the mean and the 
standard deviation.

For HBMs, the registries are the observations, which are the input data to get 
a prediction. The prediction, in this case, is encapsulated by the parameter
of the priors. This leads to, mathematically, predict by using the posterior 
distribution, meaning that:

\begin{equation}
  P(\mu|R_i) = \frac{P(R_i|\mu) P(\mu)}{P(R_i)}
  \label{eqn:hierarchical_posterior}
\end{equation}

According to the equation \ref{eqn:hierarchical_posterior}, the probability of
the parameter $\mu$ given $R_i$ is used to predict the most feasible 
probability of the hypothesis $\mu$ given the observations ($R_i$), following
the logic stated in the last section.

HBM also can have more than three layers, meaning that the parameters can also
generate other parameters going to the upper layers (closer to the hyper-priors).
To define an example, suppose that the hierarchy is defined as follows:

\begin{itemize}
  \item Observations $R_i$
  \item Parameters - layer 1 $\theta_i$: which are the means of the distribution
  of $\hat{R_i}$, where the hat defines the probability or the approximation of 
  $R_i$.
  \item Parameters - layer 2 $\phi$: which is the hyper-parameter space.
  \item Hyper-priors: which describes the PDFs for $\phi$.
\end{itemize}

This structure is also called 2-stage hierarchical HBM. To predict the 
hypothesis, the hyper-parameters are also included as a conditional probability
given the observations, such that the posterior equation is \cite{ref4}:

\begin{equation}
  P(\theta_i, \phi|R_i) = \frac{P(R_i|\theta_i) P(\theta_i | \phi) P(\phi)}{P(R_i)}
  \label{eqn:hierarchical_posterior_2stage}
\end{equation}

Equation \ref{eqn:hierarchical_posterior_2stage} shows how the interconnection
between the several nodes of the hierarchy interacts each other, by means of 
their conditional probabilities.

\subsection{Training an HBM}

The training process of an HBM takes advantage of the Bayes Theorem property for
swapping the likelihood when the posterior distribution is known. Furthermore, the 
process is spread out from the hyper-parameters and hyper-priors to the 
observations, to adjust the several PDFs of the parameters which describe the 
HBM.

Following the 2-stage hierarchical HBM, the stages of training can be defined by
following a series of steps per stage:

\begin{itemize}
  \item Stage I: $R_i | \theta_i, \phi \sim P(R_i | \theta_i, \phi)$
  \item Stage II: $\theta_i | \phi \sim P(\theta_i | \phi)$
  \item Stage III: $\phi \sim P(\phi)$
\end{itemize}

where $\sim$ indicates "has the distribution of".

In the first stage of training, the parameter distribution of $\theta_i$ is 
computed by using the training observations. When the process of stage I 
finishes, the next step is to train the distribution of $\phi$ by the already
seen parameters $\theta_i$ and it's behaviour when training the stage I. The 
process concludes by defining the hyper-prior distribution of the 
hyper-parameter $\phi$ \cite{ref0}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Computation of HBMs and challenges}

Allenby, Rossi and McCulloch explains in \cite{ref0} that dealing with the 
equation of the gaussian distribution is computationally  expensive. The way to 
tackle the computational cost of HBMs is by using iterative methods which 
converge to a solution closer to the intended solution. One of the most popular
methods is the Markov Chain Monte Carlo (MCMC). 

For estimating one of the distributions, the algorithm consists of the following
steps:

\begin{enumerate}
  \item Draw the $\mu$ given the data ${y_i, x_i}$ and the most recent $\sigma$.
  \item Draw the $\sigma$ given the data ${y_i, x_i}$ and the most recent $\mu$.
  \item Repeat until a change threshold or a certain number of iterations.
\end{enumerate}

For HBMs purposes, the process is quite similar when inferring the parameters
during the training process.

For computing the distribution during the training, the authors had to run 6000 
iterations to get a convergence, suggesting that it can be computationally expensive
due to its iterative nature.

The authors also advice that HBMs do not converge in a close-form solution 
as most of the classical ML methods, since its nature is purely probabilistic.
This means that each time when the HBM training and prediction are run, different
results may be expected. Despite that, the results are also constrained by the
PDFs involved in the hierarchy.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{A HBM example}

Different to the other Machine Learning techniques, HBMs can handle the errors
and uncertainties from the measurements and deal with them statistically, since
its basis relies on Statistics. This fact makes HBMs useful for experimental
data, where there is tolerance in the measurements given by an instrument.
Also, since the HBMs are based on Bayesian inference, the idea is to forecast
values from non-existent observations, such as predict the market when lowering
the prices and increasing the costs.

\cite{ref4} illustrates the usage of PyStan library by an example of radon 
measurement on households from the uranium concentration observed from 
different measurement techniques performed on the same household. The basic
idea is that there is a proven correlation between the uranium level and the
radon. Then, from measurements of uranium, it will be possible to determine
the concentration of radon.

The correlation between radon and uranium is given by equation \ref{eqn:radon}.

\begin{equation}
  \log(\rho_{\text{radon}}) \propto \rho_{\text{uranium}}
  \label{eqn:radon}
\end{equation}

where $\rho$ is the concentration.

The dataset provides the following details:

\begin{itemize}
  \item $x$: number of floor of the household
  \item $y$: $\log(\rho_{\text{radon}})$
  \item $u$: $\rho_{\text{uranium}}$
  \item county: the class of measurement
  \item N: number of observations
  \item J: number of counties
\end{itemize}

Additionally, the observations ($y$) and ($u$) have error values associated 
with the measurements. The idea is to infer the radon levels from new measurements
of uranium. 

\begin{figure}[!h]
  \centering
  \includegraphics[width=\columnwidth]{../img/goal_radon.png}
  \caption{Inference of the linear regression from the uranium levels and 
  the values got from the countries. The blue dots are the values estimated
  by each county}.
  \label{fig:radon_uranium}
\end{figure}

By using HBM, it is possible to adjust the regression line from the several
observations treating them not as an average but as a probabilistic 
distribution. Averaging the counties will lead to information loss, worsening
the generalisation.

In a few words, HBMs allows to generalise a model by taking into account
measurements provided by different instruments, methodologies, etc. Also, it
allows to make the most from the data, especially, the measurement errors to
adjust the generalisation and enhance the statistics behind that
generalisation.

Therefore, the HBMs are likely to be applied when the dataset:

\begin{itemize}
  \item Have measurements (observations) from different instruments.
  \item Have errors associated with the measurements.
  \item Have several parameters linked to the output.
  \item Have errors associated with the inputs.
\end{itemize}

About the linearity, the examples provided by the framework in equation 
\ref{eqn:linearbase} is one of the possible models. HBMs can be generalised, but
the complexity of representing the distributions increases as well.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Library survey}

There are several libraries to approach HBMs. This paper focuses on only a
couple of them: PyMC3 and Stan.

\subsection{PyMC3}

\emph{PyMC3} is a Python library which describes the models in a graph-oriented
mode, similarly to one of the models of TensorFlow or PyTorch. A user already
familiar with any of those ML frameworks will find PyMC3 easy to use 
\cite{ref5}.

\begin{lstlisting}[language=Python,basicstyle=\fontsize{7}{8}\selectfont]
# Open a context:
with pm.Model() as hierarchical_model:
  # global model parameters
  alpha_mu_tmp = pm.Normal('alpha_mu_tmp', mu=0., sd=100)
  alpha_sigma_tmp = pm.HalfNormal('alpha_sigma_tmp', 5.)
  beta_mu = pm.Normal('beta_mu', mu=0., sd=100)
  beta_sigma = pm.HalfNormal('beta_sigma', 5.)

  # train type specific model parameters
  alpha_tmp = pm.Normal('alpha_tmp', mu=alpha_mu_tmp, 
                        sd=alpha_sigma_tmp, 
                        shape=n_train_types)
  # Intercept for each train type, 
  # distributed around train type mean 
  beta = pm.Normal('beta', mu=beta_mu, sd=beta_sigma, 
                   shape=n_train_types)
  # Model error
  eps = pm.HalfCauchy('eps', 5.)

  fare_est = alpha_tmp[train_type_idx] + 
             beta[train_type_idx] * 
             data.fare_encode.values

  # Data likelihood
  fare_like = pm.Normal('fare_like', mu=fare_est, 
                        sd=eps, observed=data.price)
\end{lstlisting}

According to figure \ref{fig:partial_pooled}, the upper node is represented
by \emph{alpha\_mu\_tmp} ($\mu_\alpha$), \emph{alpha\_sigma\_tmp} ($\sigma_\alpha$)
and \emph{beta\_mu\_tmp} ($\mu_\beta$), \emph{beta\_sigma\_tmp} ($\sigma_\beta$).
Then, the so-called \emph{train type-specific model parameters} are the class
parameters.

Finally, \emph{fare\_est} helps to reconstruct the model, as the equation 
\ref{eqn:linearbase} did as a base framework. For the prediction, the 
\emph{fare\_like} establishes the prediction framework, specifying that the
model sets the average observation and \emph{eps} sets the variance of the
observations because of errors associated with the instrument, for example.

\subsection{Stan}

Stan is a library available in several languages, and it has a Python wrapper to
easy the job of code prototyping for proofs-of-concept (PyStan). The models are 
specified through a C++-like style by using a string. The model is compiled
under-the-hood by Stan and made available to Python again \cite{ref4}.

To declare a model for the radon example:

\begin{lstlisting}[language=Python,basicstyle=\fontsize{7}{8}\selectfont]
partial_pooling = """
data {
  int<lower=0> N;
  int<lower=1,upper=85> county[N];
  vector[N] y;
}
parameters {
  vector[85] a;
  real mu_a;
  real<lower=0,upper=100> sigma_a;
  real<lower=0,upper=100> sigma_y;
}
transformed parameters {
  vector[N] y_hat;
  for(i in 1:N)
    y_hat[i] <- a[county[i]];
}
model {
  mu_a ~ normal(0, 1);
  a ~ normal(10 * mu_a, sigma_a);
  
  y ~ normal(y_hat, sigma_y);
}
"""
\end{lstlisting}

For PyStan, the representation is more mathematic than PyMC3. It is possible
to notice that the model is represented by layers: the first indicates that
the mean of $\mu_\alpha$ is distributed in a standard distribution. Then,
$\alpha$ will be distributed in a normal distribution with mean $\mu_\alpha$
and $\sigma_\alpha$. Then, the observations will be distributed in normal
distributions with mean equal to the transformed variable and $\sigma_y$.

For the transformed variables, Stan opens a structure called 
\emph{transformed parameters}, whose number of elements will be the number of
factors or classes.

Another interesting fact is that it declares the data in the first structure,
specifying the number of classes, and the observations.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Conclusion}

HBMs is another method for machine learning based on Bayesian Inference
which provides support for experimental data, allowing the user to handle 
errors associated to the inputs and outputs of an experiment.

One of its key characteristics is that it can handle measurements provided
by different instruments, strengthening the generalisation of the data
independently of the instrument, taking into account all the characteristics
about the distribution of the measurements and avoiding data loss by averaging
the measurements directly (pooled model).

HBMs can be considered as a powerful forecast tool to predict effects on 
several contexts, from Economics (predicting the impact of lowering the prices
of a product given other experiments done on other products) to Science, 
generalising the relationship between an intermediate measurement to a final
measurement (radon concentration from uranium).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{thebibliography}{99}

\bibitem{ref0} Allenby, Rossi, McCulloch (January 2005). 
               "Hierarchical Bayes Model: A Practitioner’s Guide". 
               Journal of Bayesian Applications in Marketing, pp. 1–4. 
               Retrieved 6 July 2020, p. 3
\bibitem{ref1} Stuart, A.; Ord, K. (1994), 
               Kendall's Advanced Theory of Statistics: Volume I—Distribution Theory, 
               Edward Arnold
\bibitem{ref2} Box, G. E. P. and Tiao, G. C. (1973) 
               Bayesian Inference in Statistical Analysis, 
               Wiley, ISBN 0-471-57428-7
\bibitem{ref3} Gelman, Andrew; Carlin, John B.; Stern, Hal S. \& Rubin, Donald B. (2004), 
               Bayesian Data Analysis (second ed). Boca Raton, 
               Florida: CRC Press.
\bibitem{ref4} Box G. E. P., Tiao G. C. (1965). 
               "Multiparameter problem from a bayesian point of view". 
               Multiparameter Problems From A Bayesian Point of View 
               Volume 36 Number 5. New York City: John Wiley \& Sons, 
               ISBN 0-471-57428-7
\bibitem{ref4} Chris Fonnesbeck. (w.d). 
               "A Primer on Bayesian Multilevel Modeling using PyStan".
               Retrieved 11 July 2020.
               From: https://mc-stan.org/users/documentation/case-studies/radon.html
\bibitem{ref5} Danne Elbers, Thomas Wiecki. (2016).
               "GLM: Hierarchical Linear Regression"
               Retrieved 11 July 2020.
               From: https://docs.pymc.io/notebooks/GLM-hierarchical.html
\end{thebibliography}




\end{document}
